#!/bin/sh

#rm -v top-1million-sites.csv.zip
#rm -v top-1m.csv

#wget https://statvoo.com/dl/top-1million-sites.csv.zip
#unzip https://statvoo.com/dl/top-1million-sites.csv.zip

# Uses GNU Parallel so we can run multiple jobs at once
shuf top-1m.csv | awk -F"," '{print $2}' | parallel --jobs 8 "echo {} && drill {} | grep 'Query time' && echo ''"
